package com.example.btapoop;

import java.time.LocalDate;
import java.util.Scanner;

public class TestCandidate {

    public void Candidate() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the number of candidates: ");
        int n = scanner.nextInt();

        Candidate[] danhSachThiSinh = new Candidate[n];

        // Nhập thông tin về các thí sinh
        for (int i = 0; i < n; i++) {
            System.out.println("Enter candidate information " + (i + 1) + ":");

            System.out.println("Enter id: ");
            int ma = scanner.nextInt();

            System.out.println("Enter name: ");
            String ten = scanner.next();

            System.out.println("Enter dob (yyyy-mm-dd): ");
            String ngaySinhStr = scanner.next();
            LocalDate ngaySinh = LocalDate.parse(ngaySinhStr);

            System.out.println("Enter Math score: ");
            double diemToan = scanner.nextDouble();

            System.out.println("Enter Literature score: ");
            double diemVan = scanner.nextDouble();

            System.out.println("Enter English score: ");
            double diemAnh = scanner.nextDouble();

            danhSachThiSinh[i] = new Candidate(ma, ten, ngaySinh, diemToan, diemVan, diemAnh);
        }

        // In thông tin các thí sinh có tổng điểm lớn hơn 1
        System.out.println("Information about candidates with a total score greater than 1:");
        for (Candidate candidate : danhSachThiSinh) {
            double tongDiem = candidate.getDiemToan() + candidate.getDiemVan() + candidate.getDiemAnh();
            if (tongDiem > 1) {
                System.out.println("Id: " + candidate.getId());
                System.out.println("Name: " + candidate.getName());
                System.out.println("Dob: " + candidate.getDob());
                System.out.println("ToTal: " + tongDiem);
                System.out.println();
            }
        }
    }
}
