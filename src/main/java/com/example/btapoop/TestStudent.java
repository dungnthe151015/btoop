package com.example.btapoop;

import java.time.LocalDate;
import java.util.*;

public class TestStudent {
    ArrayList<Student> listStudent = new ArrayList<>();
    Scanner scanner = new Scanner(System.in);

    public void student() {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            displayMenu();
            selectOption();
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    addStudent();
                    break;
                case 2:
                    editStudent();
                    break;
                case 3:
                    deleteStudentById();
                    break;
                case 4:
                    sortStudentByGpa();
                    break;
                case 5:
                    sortStudentByName();
                    break;
                case 6:
                    showStudent();
                    break;
                case 7:
                    System.exit(0);
            }


        }
    }

    private void addStudent() {
        System.out.println("Enter id: ");
        int id = scanner.nextInt();

                System.out.println("Enter name: ");
                String name = scanner.next();

                System.out.println("Enter age: ");
                int age = scanner.nextInt();

                System.out.println("Enter address: ");
                String address = scanner.next();

                System.out.println("Enter gpa: ");
                int gpa = scanner.nextInt();

              Student student = new Student(id,name,age,address,gpa);

                listStudent.add(student);
                System.out.println("Add Student Sucessfully!!!!");


    }
    private void editStudent() {
        if (listStudent.isEmpty()) {
            System.out.println("List student is empty !!!");
        }
        //check student by id
        System.out.println("Enter Id Student want edit: ");
        int idFinding = scanner.nextInt();

        for (Student student : listStudent) {
            if (student.getId() == idFinding){

                System.out.println("Enter name: ");
                String name = scanner.next();

                System.out.println("Enter age: ");
                int age = scanner.nextInt();

                System.out.println("Enter address: ");
                String address = scanner.next();

                System.out.println("Enter gpa: ");
                int gpa = scanner.nextInt();

                student.setName(name);
                student.setAge(age);
                student.setAddress(address);
                student.setGpa(gpa);
                System.out.println("Update Student Sucessfully!!!!");
            }else{
                System.out.println("Not found Student");
            }

        }
    }
    private void deleteStudentById() {
        if (listStudent.isEmpty()) {
            System.out.println("List student is empty !!!");
        }
        System.out.println("Enter Id Student want delete: ");
        int idFinding = scanner.nextInt();
        for (Student student : listStudent) {
            if (student.getId() == idFinding){
                listStudent.remove(student);
                System.out.println("Remove Student Sucessfully!!!!");
                return;
            }else{
                System.out.println("Not found Student");
            }

        }

    }
    private void sortStudentByGpa() {
        if (listStudent.isEmpty()) {
            System.out.println("List student is empty !!!");
        }
        // Display the list of students before sorting
        System.out.println("List of Students before sorting by GPA:");
        displayStudents(listStudent);

        // Sort the list of students by GPA
        Collections.sort(listStudent, Comparator.comparingDouble(Student::getGpa).reversed());

        // Display the list of students after sorting by GPA
        System.out.println("\nList of Students after sorting by GPA:");
        displayStudents(listStudent);

    }
    private void sortStudentByName() {
        if (listStudent.isEmpty()) {
            System.out.println("List student is empty !!!");
        }
        // Display the list of students before sorting
        System.out.println("List of Students before sorting by name:");
        displayStudents(listStudent);

        // Sort the list of students by name
        Collections.sort(listStudent, Comparator.comparing(Student::getName));

        // Display the list of students after sorting by name
        System.out.println("\nList of Students after sorting by name:");
        displayStudents(listStudent);
    }

    private void showStudent() {
        if (listStudent.isEmpty()) {
            System.out.println("List student is empty !!!");
        }
        System.out.println("List of Students:");
        for (Student student : listStudent) {
            System.out.println("\nStudent ID: " + student.getId());
            System.out.println("Name: " + student.getName());
            System.out.println("Age: " + student.getAge());
            System.out.println("Address: " + student.getAddress());
            System.out.println("GPA: " + student.getGpa());
        }

    }

    // Method to display information of students in the list
    public static void displayStudents(ArrayList<Student> students) {
        for (Student student : students) {
            System.out.println("ID: " + student.getId() + ", Name: " + student.getName() + "Age: " + student.getAge() + "Adress: "+ student.getAddress() + ", GPA: " + student.getGpa());
        }
        System.out.println();
    }

    private static void selectOption() {
        System.out.println("Enter your option: \", \"Option[1-5]");
    }

    private static void displayMenu() {
        System.out.println("1.Add a Student: ");
        System.out.println("2.Edit Student: ");
        System.out.println("3.Delete Student By Id: ");
        System.out.println("4.Sort Student By GPA: ");
        System.out.println("5.Sort Student By Name: ");
        System.out.println("6.Show Student: ");
        System.out.println("5.Exit");
    }


}
