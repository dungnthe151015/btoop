package com.example.btapoop;


import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Controller controller = new Controller();

        while (true) {
            displayMenu();
            selectOption();
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    controller.bai1();
                    break;
                case 2:
                    controller.bai2();
                    break;
                case 3:
                    controller.bai3();
                    break;
                case 5:
                    System.exit(0);
            }


        }

    }

    private static void selectOption() {
        System.out.println("Enter your option: \", \"Option[1-4]");
    }

    private static void displayMenu() {
        System.out.println("1.Bai 1");
        System.out.println("2.Bai 2");
        System.out.println("3.Bai 3");
        System.out.println("4.Exit");
    }
}
