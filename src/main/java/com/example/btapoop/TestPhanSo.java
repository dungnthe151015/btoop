package com.example.btapoop;

import java.util.Scanner;

public class TestPhanSo {
    public void phanSo() {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Enter the numerator for the first fraction: ");
        int tuSo1 = scanner.nextInt();
        System.out.println("Enter the denominator for the first fraction: ");
        int mauSo1 = scanner.nextInt();

        System.out.println("Enter the numerator for the second fraction: ");
        int tuSo2 = scanner.nextInt();
        System.out.println("Enter the denominator for the second fraction: ");
        int mauSo2 = scanner.nextInt();

        // Thực hiện các phép toán
        String tong = congPhanSo(tuSo1, mauSo1, tuSo2, mauSo2);
        String hieu = truPhanSo(tuSo1, mauSo1, tuSo2, mauSo2);
        String tich = nhanPhanSo(tuSo1, mauSo1, tuSo2, mauSo2);
        String thuong = chiaPhanSo(tuSo1, mauSo1, tuSo2, mauSo2);

        System.out.println("Sum of two fractions: " + tong);
        System.out.println("Difference of two fractions: " + hieu);
        System.out.println("Multiply two fractions: " + tich);
        System.out.println("divide two fractions: " + thuong);
    }

    // Tìm ước chung lớn nhất của hai số
    public static int timUCLN(int a, int b) {
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        return a;
    }

    // Tính tổng hai phân số
    public static String congPhanSo(int ts1, int ms1, int ts2, int ms2) {
        int ts = ts1 * ms2 + ts2 * ms1;
        int ms = ms1 * ms2;
        int ucln = timUCLN(ts, ms);
        return (ts / ucln) + "/" + (ms / ucln);
    }

    // Tính hiệu hai phân số
    public static String truPhanSo(int ts1, int ms1, int ts2, int ms2) {
        int ts = ts1 * ms2 - ts2 * ms1;
        int ms = ms1 * ms2;
        int ucln = timUCLN(ts, ms);
        return (ts / ucln) + "/" + (ms / ucln);
    }

    // Tính tích hai phân số
    public static String nhanPhanSo(int ts1, int ms1, int ts2, int ms2) {
        int ts = ts1 * ts2;
        int ms = ms1 * ms2;
        int ucln = timUCLN(ts, ms);
        return (ts / ucln) + "/" + (ms / ucln);
    }

    // Tính thương hai phân số
    public static String chiaPhanSo(int ts1, int ms1, int ts2, int ms2) {
        int ts = ts1 * ms2;
        int ms = ms1 * ts2;
        int ucln = timUCLN(ts, ms);
        return (ts / ucln) + "/" + (ms / ucln);
    }
}
